<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cristal;

class CristalController extends Controller
{
    public function getCristal() {
        return response()->json(Cristal::all(), 200);
    }
}
